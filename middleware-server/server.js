import express from 'express';
import bodyParser from 'body-parser';
import methodOverride from 'method-override';
import authenticationApi from './api/authentication';
import categoryApi from './api/category';
import checkoutApi from './api/checkout';
import productApi from './api/product';
import fileApi from './api/file';

const app = express();
const port = process.env.PORT || 9000;

//const storeName = 'elektraqapre';
//const domainName = 'elektraqapre.vtexcommercestable.com.br';
const storeName = 'elektra';
const domainName = 'elektra.vtexcommercestable.com.br';

let Router = express.Router();
let enableCrossDomain = (req, res, next) => {
	res.header('Access-Control-Allow-Origin', '*');
	res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
	res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
	res.header("Access-Control-Allow-Credentials", "true");
	next();
};

app.use(bodyParser.urlencoded({ extended: false }))
	.use(bodyParser.json())
	.use(bodyParser.json({ type: 'application/vnd.api+json' }))
	.use(methodOverride('X-HTTP-Method-Override'))
	.use(enableCrossDomain)
	.use(Router);

authenticationApi(app, storeName, domainName);
categoryApi(app, storeName, domainName);
checkoutApi(app, storeName, domainName);
productApi(app, storeName, domainName);
fileApi(app, storeName, domainName);

app.get('*', (req, res) => {
	res.send('VINNEREN-MARKETPLACE-MIDDLEWARE!!!');
}).listen(port, () => {
	console.log(`[APP] Listening on port => ${port}`)
});
