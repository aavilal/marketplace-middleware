import axios from 'axios';

module.exports = (app, storeName, domainName) => {

  app.get('/api/authentication/start', (req, res) => {
    const { cookie } = req.headers;
    //let urlvtex = 'https://' + domainName + '/api/vtexid/pub/authentication/start?callbackUrl=https%3A%2F%2F' + domainName + '%2Fapi%2Fvtexid%2Fpub%2Fauthentication%2Ffinish&scope=' + storeName + '&user=&locale=en-MX&accountName=&returnUrl=https%253A%252F%252F' + domainName + 'https%253A%252F%252F' + domainName + '%252Fadmin&appStart=true';
    let urlvtex = 'https://' + domainName + '/api/vtexid/pub/authentication/start?callbackUrl=https%3A%2F%2F' + domainName + '%2Fapi%2Fvtexid%2Fpub%2Fauthentication%2Ffinish&scope=' + storeName + '&user=&locale=es-MX&accountName=&returnUrl=%252F&appStart=true'
    let resobj;

    axios.request({
      url: urlvtex,
      method: 'get',
      headers: {
        Cookie: cookie
      }
    }).then((response) => {
      resobj = {
        success: true,
        data: response.data
      }
      res.status(200);
      res.setHeader('Content-Type', 'application/json');
      if (response.headers['set-cookie'])
        res.setHeader('set-cookie', JSON.stringify(response.headers['set-cookie']));
      res.send(JSON.stringify(resobj));
    }).catch((err) =>{
      resobj = {
        success: false,
        data: err
      }
      res.status(200);
      res.setHeader('Content-Type', 'application/json');
      res.send(JSON.stringify(resobj));
    });

  });

  app.post('/api/authentication/accesskey/send', (req, res) => {
    const { cookie } = req.headers;
    const { token, email } = req.body;
    let urlvtex = 'https://' + domainName + '/api/vtexid/pub/authentication/accesskey/send?recaptcha=&authenticationToken=' + token + '&email=' + email;
    let resobj;

    axios.request({
      url: urlvtex,
      method: 'post',
      data: {
        authenticationToken: token,
        email: email,
        method: 'POST'
      },
      headers: {
        Cookie: cookie
      }
    }).then((response) => {
      resobj = {
        success: true,
        data: response.data
      }
      res.status(200);
      res.setHeader('Content-Type', 'application/json');
      if (response.headers['set-cookie'])
        res.setHeader('set-cookie', JSON.stringify(response.headers['set-cookie']));
      res.send(JSON.stringify(resobj));
    }).catch((err) =>{
      resobj = {
        success: false,
        data: err
      }
      res.status(200);
      res.setHeader('Content-Type', 'application/json');
      res.send(JSON.stringify(resobj));
    });
  });

  app.post('/api/authentication/accesskey/validate', (req, res) => {
    const { cookie } = req.headers;
    const { token, email, key } = req.body;
    let urlvtex = 'https://' + domainName + '/api/vtexid/pub/authentication/accesskey/validate?login=' + email + '&accesskey=' + key + '&authenticationToken=' + token;
    let resobj;
    axios.request({
      url: urlvtex,
      method: 'post',
      data: {
        login: email,
        accesskey: key,
        authenticationToken: token,
        method: 'POST'
      },
      headers: {
        Cookie: cookie
      }
    }).then((response) => {
    	resobj = {
        success: true,
        data: response.data
      }
      res.status(200);
      res.setHeader('Content-Type', 'application/json');
      if (response.headers['set-cookie'])
        res.setHeader('set-cookie', JSON.stringify(response.headers['set-cookie']));
      res.send(JSON.stringify(resobj));
    }).catch((err) =>{
      resobj = {
        success: false,
        data: err
      }
      res.status(200);
      res.setHeader('Content-Type', 'application/json');
      res.send(JSON.stringify(resobj));
    });
  });

  app.get('/api/authentication/sessions', (req, res) => {
    const { cookie } = req.headers;
    let urlvtex = 'https://' + domainName + '/api/sessions?items=*';
    //let urlvtex = 'https://' + domainName + '/api/sessions/' POST;
    let resobj;
    axios.request(urlvtex, {
      url: urlvtex,
      method: 'get',
      headers: {
        Cookie: cookie
      }
    }).then((response) => {
      resobj = {
        success: true,
        data: response.data
      }
      res.status(200);
      res.setHeader('Content-Type', 'application/json');
      if (response.headers['set-cookie'])
        res.setHeader('set-cookie', JSON.stringify(response.headers['set-cookie']));
      res.send(JSON.stringify(resobj));
    }).catch((err) => {
      resobj = {
        success: false,
        data: err
      }
      res.status(200);
      res.setHeader('Content-Type', 'application/json');
      res.send(JSON.stringify(resobj));
    });
  });

  app.get('/api/authentication/get-profile', (req, res) => {
    const { cookie } = req.headers;
    let urlvtex = 'https://' + domainName + '/no-cache/profileSystem/getProfile';
    let resobj;
    axios.request({
      url: urlvtex,
      method: 'get',
      headers: {
        Cookie: cookie
      }
    }).then((response) => {
      resobj = {
        success: true,
        data: response.data
      }
      res.status(200);
      res.setHeader('Content-Type', 'application/json');
      if (response.headers['set-cookie'])
        res.setHeader('set-cookie', JSON.stringify(response.headers['set-cookie']));
      res.send(JSON.stringify(resobj));
    }).catch((err) =>{
      resobj = {
        success: false,
        data: err
      }
      res.status(200);
      res.setHeader('Content-Type', 'application/json');
      if (response.headers['set-cookie'])
        res.setHeader('set-cookie', JSON.stringify(response.headers['set-cookie']));
      res.send(JSON.stringify(resobj));
    });
  });

}
