import axios from 'axios';

module.exports = (app, storeName, domainName) => {

  app.post('/api/product/search', (req, res) => {
    const { cookie } = req.headers;
    const { search, params } = req.body;
    let pathparams = search != null ? ('/' + encodeURIComponent(search)) : '';

    let urlvtex = 'https://' + domainName + '/api/catalog_system/pub/products/search' + pathparams + params;

    let resobj;
    axios.request({
      url: urlvtex,
      method: 'get',
      headers: {
        Cookie: cookie
      }
    }).then((response) => {
      var total = 0;
      var start = 0;
      var end = 0;
      try {
        if (response.headers['resources']) {
            total = parseInt(response.headers['resources'].split('/')[1]);
            start = parseInt(response.headers['resources'].split('/')[0].split('-')[0]);
            end = parseInt(response.headers['resources'].split('/')[0].split('-')[1]);
        }
      } catch(err) {}

      resobj = {
        success: true,
        page: {
          start: start,
          end: end,
          total: total
        },
        data: response.data
      }
      res.status(200);
      res.setHeader('Content-Type', 'application/json');
      if (response.headers['set-cookie'])
        res.setHeader('set-cookie', JSON.stringify(response.headers['set-cookie']));
      res.send(JSON.stringify(resobj));
    }).catch((err) =>{
      resobj = {
        success: false,
        data: err
      }
      res.status(200);
      res.setHeader('Content-Type', 'application/json');
      res.send(JSON.stringify(resobj));
    });
  });

  app.post('/api/facets/search', (req, res) => {
    const { cookie } = req.headers;
    const { search, params } = req.body;
    let pathparams = search != null ? ('/' + encodeURIComponent(search)) : '';

    let urlvtex = 'https://' + domainName + '/api/catalog_system/pub/facets/search/' + pathparams + params;

    let resobj;
    axios.request({
      url: urlvtex,
      method: 'get',
      headers: {
        Cookie: cookie
      }
    }).then((response) => {
      resobj = {
        success: true,
        data: response.data
      }
      res.status(200);
      res.setHeader('Content-Type', 'application/json');
      if (response.headers['set-cookie'])
        res.setHeader('set-cookie', JSON.stringify(response.headers['set-cookie']));
      res.send(JSON.stringify(resobj));
    }).catch((err) =>{
      resobj = {
        success: false,
        data: err
      }
      res.status(200);
      res.setHeader('Content-Type', 'application/json');
      res.send(JSON.stringify(resobj));
    });
  });

}
