import axios from 'axios';

function getOrderFormId(requestCookie) {
  let orderFormId = '';
  let cookiesElements = requestCookie.split(';');
  for (let i = 0; i < cookiesElements.length; i++) {
    if (cookiesElements[i].indexOf('checkout.vtex.com') != -1) {
      return (cookiesElements[i].split('=')[2]);
    }
  }
  return '';
}

module.exports = (app, storeName, domainName) => {

  app.get('/api/checkout/postal-code/MEX', (req, res) => {
    const { cookie } = req.headers;
    const { zipcode } = req.body;
    let urlvtex = 'https://' + domainName + '/api/checkout/pub/postal-code/MEX/' + zipcode;
    let resobj;
    axios.request({
      url: urlvtex,
      method: 'get',
      headers: {
        Cookie: cookie
      }
    }).then((response) => {
      resobj = {
        success: true,
        data: response.data
      }
      res.status(200);
      res.setHeader('Content-Type', 'application/json');
      if (response.headers['set-cookie'])
        res.setHeader('set-cookie', JSON.stringify(response.headers['set-cookie']));
      res.send(JSON.stringify(resobj));
    }).catch((err) =>{
      resobj = {
        success: false,
        data: err
      }
      res.status(200);
      res.setHeader('Content-Type', 'application/json');
      res.send(JSON.stringify(resobj));
    });
  });

  app.get('/api/checkout/orderform', (req, res) => {
    const { cookie } = req.headers;
    let urlvtex = 'https://' + domainName + '/api/checkout/pub/orderForm';
    let resobj;
    axios.request({
      url: urlvtex,
      method: 'post',
      data: {
        expectedOrderFormSections: ["items","totalizers","clientProfileData","shippingData","paymentData","sellers","messages","marketingData","clientPreferencesData","storePreferencesData","giftRegistryData","ratesAndBenefitsData","openTextField","commercialConditionData","customData"]
      },
      headers: {
        Cookie: cookie
      }
    }).then((response) => {
      resobj = {
        success: true,
        data: response.data
      }
      res.status(200);
      res.setHeader('Content-Type', 'application/json');
      if (response.headers['set-cookie'])
        res.setHeader('set-cookie', JSON.stringify(response.headers['set-cookie']));
      res.send(JSON.stringify(resobj));
    }).catch((err) =>{
      resobj = {
        success: false,
        data: err
      }
      res.status(200);
      res.setHeader('Content-Type', 'application/json');
      res.send(JSON.stringify(resobj));
    });
  });

  app.post('/api/checkout/orderform/item/add', (req, res) => {
    const { cookie } = req.headers;
    //{id: "851557", seller: "1", quantity: 3}
    const { items } = req.body;
    let orderFormId = getOrderFormId(cookie);
    let urlvtex = 'https://' + domainName + '/api/checkout/pub/orderForm/' + orderFormId + '/items?sc=1';
    let resobj;

    axios.request({
      url: urlvtex,
      method: 'post',
      data: {
        expectedOrderFormSections: ["items","totalizers","clientProfileData","shippingData","paymentData","sellers","messages","marketingData","clientPreferencesData","storePreferencesData","giftRegistryData","ratesAndBenefitsData","openTextField","commercialConditionData","customData"],
        orderItems: items
      },
      headers: {
        Cookie: cookie
      }
    }).then((response) => {
      resobj = {
        success: true,
        data: response.data
      }
      res.status(200);
      res.setHeader('Content-Type', 'application/json');
      if (response.headers['set-cookie'])
        res.setHeader('set-cookie', JSON.stringify(response.headers['set-cookie']));
      res.send(JSON.stringify(resobj));
    }).catch((err) =>{
      resobj = {
        success: false,
        data: err
      }
      res.status(200);
      res.setHeader('Content-Type', 'application/json');
      res.send(JSON.stringify(resobj));
    });
  });

  app.post('/api/checkout/orderform/item/update', (req, res) => {
    const { cookie } = req.headers;
    //{id: "851557", seller: "1", quantity: 0, index: 1}
    const { items } = req.body;
    let orderFormId = getOrderFormId(cookie);
    let urlvtex = 'https://' + domainName + '/api/checkout/pub/orderForm/' + orderFormId + '/items/update/';
    let resobj;

    axios.request({
      url: urlvtex,
      method: 'post',
      data: {
        expectedOrderFormSections: ["items","totalizers","clientProfileData","shippingData","paymentData","sellers","messages","marketingData","clientPreferencesData","storePreferencesData","giftRegistryData","ratesAndBenefitsData","openTextField","commercialConditionData","customData"],
        noSplitItem: true,
        orderItems: items
      },
      headers: {
        Cookie: cookie
      }
    }).then((response) => {
      resobj = {
        success: true,
        data: response.data
      }
      res.status(200);
      res.setHeader('Content-Type', 'application/json');
      if (response.headers['set-cookie'])
        res.setHeader('set-cookie', JSON.stringify(response.headers['set-cookie']));
      res.send(JSON.stringify(resobj));
    }).catch((err) =>{
      resobj = {
        success: false,
        data: err
      }
      res.status(200);
      res.setHeader('Content-Type', 'application/json');
      res.send(JSON.stringify(resobj));
    });
  });

  app.post('/api/checkout/orderform/attachments/profile', (req, res) => {
    const { cookie } = req.headers;
    const { email, name, lastname, phone } = req.body;
    let orderFormId = getOrderFormId(cookie);
    let urlvtex = 'https://' + domainName + '/api/checkout/pub/orderForm/' + orderFormId + '/attachments/clientProfileData';
    let resobj;

    axios.request({
      url: urlvtex,
      method: 'post',
      data: {
        expectedOrderFormSections: ["items","totalizers","clientProfileData","shippingData","paymentData","sellers","messages","marketingData","clientPreferencesData","storePreferencesData","giftRegistryData","ratesAndBenefitsData","openTextField","commercialConditionData","customData"],
        corporateDocument: null,
        corporateName: null,
        document: null,
        documentType: 'rfc',
        email: email,
        firstEmail: email,
        firstName: name,
        lastName: lastname,
        phone: phone,
        isCorporate: false,
        tradeName: null
      },
      headers: {
        Cookie: cookie
      }
    }).then((response) => {
      resobj = {
        success: true,
        data: response.data
      }
      res.status(200);
      res.setHeader('Content-Type', 'application/json');
      if (response.headers['set-cookie'])
        res.setHeader('set-cookie', JSON.stringify(response.headers['set-cookie']));
      res.send(JSON.stringify(resobj));
    }).catch((err) =>{
      resobj = {
        success: false,
        data: err
      }
      res.status(200);
      res.setHeader('Content-Type', 'application/json');
      res.send(JSON.stringify(resobj));
    });
  });

  app.post('/api/checkout/orderform/attachments/preferences', (req, res) => {
    //optinNewsLetter = true
    const { cookie } = req.headers;
    const { newsletter } = req.body;
    let orderFormId = getOrderFormId(cookie);
    let urlvtex = 'https://' + domainName + '/api/checkout/pub/orderForm/' + orderFormId + '/attachments/clientPreferencesData';
    let resobj;

    axios.request({
      url: urlvtex,
      method: 'post',
      data: {
        expectedOrderFormSections: ["items","totalizers","clientProfileData","shippingData","paymentData","sellers","messages","marketingData","clientPreferencesData","storePreferencesData","giftRegistryData","ratesAndBenefitsData","openTextField","commercialConditionData","customData"],
        locale: "es-MX",
        optinNewsLetter: newsletter
      },
      headers: {
        Cookie: cookie
      }
    }).then((response) => {
      resobj = {
        success: true,
        data: response.data
      }
      res.status(200);
      res.setHeader('Content-Type', 'application/json');
      if (response.headers['set-cookie'])
        res.setHeader('set-cookie', JSON.stringify(response.headers['set-cookie']));
      res.send(JSON.stringify(resobj));
    }).catch((err) =>{
      resobj = {
        success: false,
        data: err
      }
      res.status(200);
      res.setHeader('Content-Type', 'application/json');
      res.send(JSON.stringify(resobj));
    });
  });

  app.post('/api/checkout/orderform/attachments/shipping', (req, res) => {
    const { cookie } = req.headers;
    const { shipping } = req.body;
    let orderFormId = getOrderFormId(cookie);
    let urlvtex = 'https://' + domainName + '/api/checkout/pub/orderForm/' + orderFormId + '/attachments/shippingData';
    let resobj;
    shipping['expectedOrderFormSections'] = ["items","totalizers","clientProfileData","shippingData","paymentData","sellers","messages","marketingData","clientPreferencesData","storePreferencesData","giftRegistryData","ratesAndBenefitsData","openTextField","commercialConditionData","customData"];
    //1
    //address = null
    //availableAddresses = []
    //pickupPoints = []
    //selectedAddresses,
    //logisticsInfo:[{"itemIndex":0,"selectedSla":"Envío domicilio","selectedDeliveryChannel":"delivery","addressId":null,"slas":[],"shipsTo":["MEX"],"itemId":"851557","deliveryChannels":[{"id":"delivery"}]}],
    //2 load preform (before zipcode)
    //clearAddressIfPostalCodeNotFound: true
    //"address":{"postalCode":"11000","city":"MIGUEL HIDALGO","state":"Ciudad de México","country":"MEX","street":"","number":"","neighborhood":"","complement":"","reference":"","geoCoordinates":[],"neighborhoods":"Lomas de Chapultepec I Sección::Lomas de Chapultepec II Sección::Lomas de Chapultepec VIII Sección::Lomas de Chapultepec VI Sección::Lomas de Chapultepec IV Sección::Lomas de Chapultepec V Sección::Lomas de Chapultepec VII Sección::Lomas de Chapultepec III Sección","receiverName":"Jimena Avila","postalCodeIsValid":true},
    //3 (el 2 devuelve addressId)
    //address:{"addressId":"6d3c0c2fab66454787cd95003c5a6c7b","addressType":"residential","city":"MIGUEL HIDALGO","complement":null,"country":"MEX","geoCoordinates":[],"neighborhood":"Lomas de Chapultepec I Sección","number":"2","postalCode":"11000","receiverName":"Jimena Avila","reference":null,"state":"CIUDAD DE MÉXICO","street":"calle"},
    //logisticsInfo:[{"itemIndex":0,"selectedSla":"Envío domicilio","selectedDeliveryChannel":"delivery"}]
    //

    axios.request({
      url: urlvtex,
      method: 'post',
      data: shipping,
      headers: {
        Cookie: cookie
      }
    }).then((response) => {
      resobj = {
        success: true,
        data: response.data
      }
      res.status(200);
      res.setHeader('Content-Type', 'application/json');
      if (response.headers['set-cookie'])
        res.setHeader('set-cookie', JSON.stringify(response.headers['set-cookie']));
      res.send(JSON.stringify(resobj));
    }).catch((err) =>{
      resobj = {
        success: false,
        data: err
      }
      res.status(200);
      res.setHeader('Content-Type', 'application/json');
      res.send(JSON.stringify(resobj));
    });
  });

  app.post('/api/checkout/orderform/attachments/payment', (req, res) => {
    //payment of paymentData.installmentOptions || paymentData.paymentSystems
    //payments = [{"paymentSystem":18,"referenceValue":3689700,"installments":1}]
    const { cookie } = req.headers;
    const { payments } = req.body;
    let orderFormId = getOrderFormId(cookie);
    let urlvtex = 'https://' + domainName + '/api/checkout/pub/orderForm/' + orderFormId + '/attachments/paymentData';
    let resobj;

    axios.request({
      url: urlvtex,
      method: 'post',
      data: {
        expectedOrderFormSections: ["items","totalizers","clientProfileData","shippingData","paymentData","sellers","messages","marketingData","clientPreferencesData","storePreferencesData","giftRegistryData","ratesAndBenefitsData","openTextField","commercialConditionData","customData"],
        giftCards: [],
        payments: payments
      },
      headers: {
        Cookie: cookie
      }
    }).then((response) => {
      resobj = {
        success: true,
        data: response.data
      }
      res.status(200);
      res.setHeader('Content-Type', 'application/json');
      if (response.headers['set-cookie'])
        res.setHeader('set-cookie', JSON.stringify(response.headers['set-cookie']));
      res.send(JSON.stringify(resobj));
    }).catch((err) =>{
      resobj = {
        success: false,
        data: err
      }
      res.status(200);
      res.setHeader('Content-Type', 'application/json');
      res.send(JSON.stringify(resobj));
    });
  });

}
