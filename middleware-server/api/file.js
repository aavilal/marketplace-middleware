import axios from 'axios';

module.exports = (app, storeName, domainName) => {

  app.get('/api/file/:filename', (req, res) => {
    const filename = req.params.filename;
    //TEST PROD
    let urlvtex = 'https://elektraqapre.vtexcommercestable.com.br/arquivos/' + filename + '.js?v=' + (new Date().getTime());
    //let urlvtex = 'https://' + domainName + '/arquivos/' + filename + '.js?v=' + (new Date().getTime());
    let resobj;

    axios.request({
      url: urlvtex,
      method: 'get',
      responseType: 'blob'
    }).then((response) => {
      resobj = {
        success: true,
        data: response.data
      }
      res.status(200);
      res.setHeader('Content-Type', 'application/json');
      res.send(JSON.stringify(resobj));
    }).catch((err) =>{
      resobj = {
        success: false
      }
      res.status(200);
      res.setHeader('Content-Type', 'application/json');
      res.send(JSON.stringify(resobj));
    });

  });

}
