import axios from 'axios';

module.exports = (app, storeName, domainName) => {

  app.get('/api/catalog/category', (req, res) => {
    //define deep tree
    const { cookie } = req.headers;
    let urlvtex = 'https://' + domainName + '/api/catalog_system/pub/category/tree/1';
    let resobj;
    axios.request({
      url: urlvtex,
      method: 'get',
      headers: {
        Cookie: cookie
      }
    }).then((response) => {
      resobj = {
        success: true,
        data: response.data
      }
      res.status(200);
      res.setHeader('Content-Type', 'application/json');
      if (response.headers['set-cookie'])
        res.setHeader('set-cookie', JSON.stringify(response.headers['set-cookie']));
      res.send(JSON.stringify(resobj));
    }).catch((err) =>{
      resobj = {
        success: false,
        data: err
      }
      res.status(200);
      res.setHeader('Content-Type', 'application/json');
      res.send(JSON.stringify(resobj));
    });
  });

}
