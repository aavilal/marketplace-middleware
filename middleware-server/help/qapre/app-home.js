{
  "@1_banner": [{
    "image": "https://elektraqapre.vtexcommercestable.com.br/arquivos/app-banner-01.jpg",
    "detail": {
      "image" : "https://elektraqapre.vtexcommercestable.com.br/arquivos/app-banner-01.jpg",
      "title": "Black Friday",
      "clusterId": 137
    }
  },{
    "image": "https://elektraqapre.vtexcommercestable.com.br/arquivos/app-banner-02.jpg",
    "detail": {
      "image" : "https://elektraqapre.vtexcommercestable.com.br/arquivos/app-banner-01.jpg",
      "title": "Black Friday",
      "clusterId": 137
    }
  },{
    "image": "https://elektraqapre.vtexcommercestable.com.br/arquivos/app-banner-03.jpg",
    "detail": {
      "image" : "https://elektraqapre.vtexcommercestable.com.br/arquivos/app-banner-01.jpg",
      "title": "Black Friday",
      "clusterId": 137
    }
  },{
    "image": "https://elektraqapre.vtexcommercestable.com.br/arquivos/app-banner-04.jpg",
    "detail": {
      "image" : "https://elektraqapre.vtexcommercestable.com.br/arquivos/app-banner-01.jpg",
      "title": "Black Friday",
      "clusterId": 137
    }
  }],
  "@2_boxBanner": {
    "title": "Week Promotion",
    "data": [{
      "image": "https://elektraqapre.vtexcommercestable.com.br/arquivos/app-box-banner-01.jpg",
      "detail": {
        "image" : "https://elektraqapre.vtexcommercestable.com.br/arquivos/app-banner-01.jpg",
        "title": "Black Friday",
        "clusterId": 137
      }
    },{
      "image": "https://elektraqapre.vtexcommercestable.com.br/arquivos/app-box-banner-02.jpg",
      "detail": {
        "image" : "https://elektraqapre.vtexcommercestable.com.br/arquivos/app-banner-01.jpg",
        "title": "Black Friday",
        "clusterId": 137
      }
    },{
      "image": "https://elektraqapre.vtexcommercestable.com.br/arquivos/app-box-banner-03.jpg",
      "detail": {
        "image" : "https://elektraqapre.vtexcommercestable.com.br/arquivos/app-banner-01.jpg",
        "title": "Black Friday",
        "clusterId": 137
      }
    },{
      "image": "https://elektraqapre.vtexcommercestable.com.br/arquivos/app-box-banner-04.jpg",
      "detail": {
        "image" : "https://elektraqapre.vtexcommercestable.com.br/arquivos/app-banner-01.jpg",
        "title": "Black Friday",
        "clusterId": 137
      }
    }]
  },
  "@3_categories": {
    "title": "Category",
    "data": [{
      "image": "https://elektraqapre.vtexcommercestable.com.br/arquivos/app-category-01.png",
      "categoryId": 1,
      "detail": {
        "image" : "https://elektraqapre.vtexcommercestable.com.br/arquivos/app-banner-01.jpg",
        "title": "Black Friday"
      }
    },{
      "image": "https://elektraqapre.vtexcommercestable.com.br/arquivos/app-category-01.png",
      "categoryId": 74,
      "detail": {
        "image" : "https://elektraqapre.vtexcommercestable.com.br/arquivos/app-banner-01.jpg",
        "title": ""
      }
    },{
      "image": "https://elektraqapre.vtexcommercestable.com.br/arquivos/app-category-01.png",
      "categoryId": 106,
      "detail": {
        "image" : "https://elektraqapre.vtexcommercestable.com.br/arquivos/app-banner-01.jpg",
        "title": "Black Friday"
      }
    },{
      "image": "https://elektraqapre.vtexcommercestable.com.br/arquivos/app-category-01.png",
      "categoryId": 165,
      "detail": {
        "image" : "https://elektraqapre.vtexcommercestable.com.br/arquivos/app-banner-01.jpg",
        "title": "Black Friday"
      }
    },{
      "image": "https://elektraqapre.vtexcommercestable.com.br/arquivos/app-category-01.png",
      "categoryId": 166,
      "detail": {
        "image" : "https://elektraqapre.vtexcommercestable.com.br/arquivos/app-banner-01.jpg",
        "title": "Black Friday"
      }
    },{
      "image": "https://elektraqapre.vtexcommercestable.com.br/arquivos/app-category-01.png",
      "categoryId": 167,
      "detail": {
        "image" : "https://elektraqapre.vtexcommercestable.com.br/arquivos/app-banner-01.jpg",
        "title": "Black Friday"
      }
    },{
      "image": "https://elektraqapre.vtexcommercestable.com.br/arquivos/app-category-01.png",
      "categoryId": 169,
      "detail": {
        "image" : "https://elektraqapre.vtexcommercestable.com.br/arquivos/app-banner-01.jpg",
        "title": "Black Friday"
      }
    },{
      "image": "https://elektraqapre.vtexcommercestable.com.br/arquivos/app-category-01.png",
      "categoryId": 170,
      "detail": {
        "image" : "https://elektraqapre.vtexcommercestable.com.br/arquivos/app-banner-01.jpg",
        "title": "Black Friday"
      }
    },{
      "image": "https://elektraqapre.vtexcommercestable.com.br/arquivos/app-category-01.png",
      "categoryId": 171,
      "detail": {
        "image" : "https://elektraqapre.vtexcommercestable.com.br/arquivos/app-banner-01.jpg",
        "title": "Black Friday"
      }
    },{
      "image": "https://elektraqapre.vtexcommercestable.com.br/arquivos/app-category-01.png",
      "categoryId": 172,
      "detail": {
        "image" : "https://elektraqapre.vtexcommercestable.com.br/arquivos/app-banner-01.jpg",
        "title": "Black Friday"
      }
    },{
      "image": "https://elektraqapre.vtexcommercestable.com.br/arquivos/app-category-01.png",
      "categoryId": 173,
      "detail": {
        "image" : "https://elektraqapre.vtexcommercestable.com.br/arquivos/app-banner-01.jpg",
        "title": "Black Friday"
      }
    },{
      "image": "https://elektraqapre.vtexcommercestable.com.br/arquivos/app-category-01.png",
      "categoryId": 174,
      "detail": {
        "image" : "https://elektraqapre.vtexcommercestable.com.br/arquivos/app-banner-01.jpg",
        "title": "Black Friday"
      }
    },{
      "image": "https://elektraqapre.vtexcommercestable.com.br/arquivos/app-category-01.png",
      "categoryId": 175,
      "detail": {
        "image" : "https://elektraqapre.vtexcommercestable.com.br/arquivos/app-banner-01.jpg",
        "title": "Black Friday"
      }
    },{
      "image": "https://elektraqapre.vtexcommercestable.com.br/arquivos/app-category-01.png",
      "categoryId": 176,
      "detail": {
        "image" : "https://elektraqapre.vtexcommercestable.com.br/arquivos/app-banner-01.jpg",
        "title": "Black Friday"
      }
    }]
  },
  "@4_productList": {
    "title": "Trending deals",
    "clusterId": 137,
    "maxItems": 5,
    "detail": {
      "image" : "https://elektraqapre.vtexcommercestable.com.br/arquivos/app-banner-01.jpg",
      "title": "Black Friday"
    }
  },
  "@5_productList": {
    "title": "Most-viewed items",
    "clusterId": 138,
    "maxItems": 5,
    "detail": {
      "image" : "https://elektraqapre.vtexcommercestable.com.br/arquivos/app-banner-01.jpg",
      "title": "Black Friday"
    }
  },
  "@6_productList": {
    "title": "You might also like",
    "clusterId": 137,
    "maxItems": 5,
    "detail": {
      "image" : "https://elektraqapre.vtexcommercestable.com.br/arquivos/app-banner-01.jpg",
      "title": "Black Friday"
    }
  },
  "@7_productList": {
    "title": "Our featured offers",
    "clusterId": 138,
    "maxItems": 5,
    "detail": {
      "image" : "https://elektraqapre.vtexcommercestable.com.br/arquivos/app-banner-01.jpg",
      "title": "Black Friday"
    }
  }
}
