{
  "@1_banner": [{
    "image": "https://elektraqapre.vtexcommercestable.com.br/arquivos/app-banner-01.jpg",
    "detail": {
      "image" : "https://elektraqapre.vtexcommercestable.com.br/arquivos/app-banner-01.jpg",
      "title": "Black Friday",
      "clusterId": 215
    }
  },{
    "image": "https://elektraqapre.vtexcommercestable.com.br/arquivos/app-banner-02.jpg",
    "detail": {
      "image" : "https://elektraqapre.vtexcommercestable.com.br/arquivos/app-banner-01.jpg",
      "title": "Black Friday",
      "clusterId": 216
    }
  },{
    "image": "https://elektraqapre.vtexcommercestable.com.br/arquivos/app-banner-03.jpg",
    "detail": {
      "image" : "https://elektraqapre.vtexcommercestable.com.br/arquivos/app-banner-01.jpg",
      "title": "Black Friday",
      "clusterId": 323
    }
  },{
    "image": "https://elektraqapre.vtexcommercestable.com.br/arquivos/app-banner-04.jpg",
    "detail": {
      "image" : "https://elektraqapre.vtexcommercestable.com.br/arquivos/app-banner-01.jpg",
      "title": "Black Friday",
      "clusterId": 267
    }
  }],
  "@2_boxBanner": {
    "title": "Week Promotion",
    "data": [{
      "image": "https://elektraqapre.vtexcommercestable.com.br/arquivos/app-box-banner-01.jpg",
      "detail": {
        "image" : "https://elektraqapre.vtexcommercestable.com.br/arquivos/app-banner-01.jpg",
        "title": "Black Friday",
        "clusterId": 519
      }
    },{
      "image": "https://elektraqapre.vtexcommercestable.com.br/arquivos/app-box-banner-02.jpg",
      "detail": {
        "image" : "https://elektraqapre.vtexcommercestable.com.br/arquivos/app-banner-01.jpg",
        "title": "Black Friday",
        "clusterId": 157
      }
    },{
      "image": "https://elektraqapre.vtexcommercestable.com.br/arquivos/app-box-banner-03.jpg",
      "detail": {
        "image" : "https://elektraqapre.vtexcommercestable.com.br/arquivos/app-banner-01.jpg",
        "title": "Black Friday",
        "clusterId": 217
      }
    },{
      "image": "https://elektraqapre.vtexcommercestable.com.br/arquivos/app-box-banner-04.jpg",
      "detail": {
        "image" : "https://elektraqapre.vtexcommercestable.com.br/arquivos/app-banner-01.jpg",
        "title": "Black Friday",
        "clusterId": 254
      }
    }]
  },
  "@3_categories": {
    "title": "Category",
    "data": [{
      "image": "https://elektraqapre.vtexcommercestable.com.br/arquivos/app-category-01.png",
      "categoryId": 1371640,
      "detail": {
        "image" : "https://elektraqapre.vtexcommercestable.com.br/arquivos/app-banner-01.jpg"
      }
    },{
      "image": "https://elektraqapre.vtexcommercestable.com.br/arquivos/app-category-01.png",
      "categoryId": 1371643,
      "detail": {
        "image" : "https://elektraqapre.vtexcommercestable.com.br/arquivos/app-banner-01.jpg"
      }
    },{
      "image": "https://elektraqapre.vtexcommercestable.com.br/arquivos/app-category-01.png",
      "categoryId": 1371652,
      "detail": {
        "image" : "https://elektraqapre.vtexcommercestable.com.br/arquivos/app-banner-01.jpg"
      }
    },{
      "image": "https://elektraqapre.vtexcommercestable.com.br/arquivos/app-category-01.png",
      "categoryId": 1371644,
      "detail": {
        "image" : "https://elektraqapre.vtexcommercestable.com.br/arquivos/app-banner-01.jpg"
      }
    },{
      "image": "https://elektraqapre.vtexcommercestable.com.br/arquivos/app-category-01.png",
      "categoryId": 1371654,
      "detail": {
        "image" : "https://elektraqapre.vtexcommercestable.com.br/arquivos/app-banner-01.jpg"
      }
    },{
      "image": "https://elektraqapre.vtexcommercestable.com.br/arquivos/app-category-01.png",
      "categoryId": 1371655,
      "detail": {
        "image" : "https://elektraqapre.vtexcommercestable.com.br/arquivos/app-banner-01.jpg"
      }
    }]
  },
  "@4_productList": {
    "title": "Trending deals",
    "clusterId": 215,
    "maxItems": 5,
    "detail": {
      "image" : "https://elektraqapre.vtexcommercestable.com.br/arquivos/app-banner-01.jpg",
      "title": "Black Friday"
    }
  },
  "@5_productList": {
    "title": "Most-viewed items",
    "clusterId": 254,
    "maxItems": 5,
    "detail": {
      "image" : "https://elektraqapre.vtexcommercestable.com.br/arquivos/app-banner-01.jpg",
      "title": "Black Friday"
    }
  },
  "@6_productList": {
    "title": "You might also like",
    "clusterId": 216,
    "maxItems": 5,
    "detail": {
      "image" : "https://elektraqapre.vtexcommercestable.com.br/arquivos/app-banner-01.jpg",
      "title": "Black Friday"
    }
  },
  "@7_productList": {
    "title": "Our featured offers",
    "clusterId": 157,
    "maxItems": 5,
    "detail": {
      "image" : "https://elektraqapre.vtexcommercestable.com.br/arquivos/app-banner-01.jpg",
      "title": "Black Friday"
    }
  }
}
